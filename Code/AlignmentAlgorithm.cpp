#include <sstream>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <algorithm>
#include <stdio.h>
#include <string.h>
#define k 15
#define w 5

using namespace std;

string referenceGenome;
vector<string> mutationSequences;
ofstream outputFile("output.csv");

string reverseString(string genome)
{
    string reversed;
    for (char n : genome)
    {
        switch (n)

        {
        case 'A':
            reversed += 'T';
            break;
        case 'T':
            reversed += 'A';
            break;
        case 'G':
            reversed += 'C';
            break;
        case 'C':
            reversed += 'G';
            break;
        };
    };
    return reversed;
}
set<pair<string, int>> calculateMinimizers(string sequence)
{
    int l = w + k - 1;
    //unique value-position elements
    set<pair<string, int>> minimizers;
    //start-minimizers
    for (int i = 1; i < w; i++)
    {
        int l2 = i + k - 1;
        //get current substring
        string window = sequence.substr(0, l2);

        //initialize to first value
        string minString = window.substr(0, k);
        int minIndex = 0;
        //find smallest substr
        for (int j = 1; j <= window.size() - k; j++)
        {
            if (window.substr(j, k) < minString)
            {
                minString = window.substr(j, k);
                minIndex = j;
            }
        }

        //get reverse complement minimizer
        string windowReversed = reverseString(window);
        string minStringReversed = windowReversed.substr(0, k);
        int minIndexReversed = 0;
        for (int j = 1; j <= windowReversed.size() - k; j++)
        {
            if (windowReversed.substr(j, k) < minStringReversed)
            {
                minStringReversed = windowReversed.substr(j, k);
                minIndexReversed = j;
            }
        }
        //get smaller of two minimizers
        if (minString < minStringReversed)
            minimizers.insert({minString, minIndex + 1});
        else
            minimizers.insert({minStringReversed, minIndexReversed + 1});
    }

    // inner and end-minimizers
    for (int i = 0; i <= sequence.size() - k; i++)
    { //get current substring
        string window = sequence.substr(i, l);
        //initialize to first value
        string minSubstr = window.substr(0, k);
        int minIndex = 0;
        //find smallest substr
        for (int j = 1; j <= window.size() - k; j++)
        {
            if (window.substr(j, k) < minSubstr)
            {
                minSubstr = window.substr(j, k);
                minIndex = j;
            }
        }
        minimizers.insert({minSubstr, minIndex + i + 1});
    }

    return minimizers;
}

//sort minimizer matches by reference index
bool sortbysec(const tuple<string, int, int> &a,
               const tuple<string, int, int> &b)
{
    return (get<1>(a) < get<1>(b));
}

//match minimizers and sort them by reference index
vector<tuple<string, int, int>> sortedMatching(set<pair<string, int>> referenceMinimizers, set<pair<string, int>> mutationMinimizers)
{
    vector<tuple<string, int, int>> matchesVect;
    //compare each minimizer to find matches
    for (pair<string, int> p1 : referenceMinimizers)
        for (pair<string, int> p2 : mutationMinimizers)
        {

            //because minimizers are sorted by string name, if p1 is smaller than some p2, surely no larger p2 will match
            //break will save time
            if (p1.first < p2.first)
                break;
            //if substrings are equal, add to matchesMap
            else if (p1.first == p2.first)
            {
                matchesVect.push_back({p1.first, p1.second, p2.second});
            }
        }

    sort(matchesVect.begin(), matchesVect.end(), sortbysec);
    return matchesVect;
}

// calculates the length of the longest increasing subsequence in vector of size n
// returns final minimizers
vector<tuple<string, int, int>> lis(vector<tuple<string, int, int>> arr)
{
    int n = arr.size();
    //counting vector
    vector<int> lis(n, 1);
    //parent index vectors, default value -1
    vector<int> parentsIndex(n, -1);
    // Compute optimized LIS values in bottom up manner
    for (int i = 1; i < n; i++)
    {
        for (int j = 0; j < i; j++)
            if (
                //second index increased
                get<2>(arr[i]) > get<2>(arr[j])
                //new lis value larger than before
                && lis[i] < lis[j] + 1
                //first index not the same
                && get<1>(arr[i]) != get<1>(arr[j])
                //limit max minimizer distance
                && abs(get<1>(arr[i]) - get<1>(arr[j])) < 500 && abs(get<2>(arr[i]) - get<2>(arr[j])) < 500)
            {
                lis[i] = lis[j] + 1;
                parentsIndex[i] = j;
            }
    }
    //get max lis index
    int max = lis.at(0);
    int maxIndex = 0;
    for (int i = 0; i < lis.size(); i++)
    {
        if (lis[i] > max)
        {
            max = lis[i];
            maxIndex = i;
        }
    }
    vector<tuple<string, int, int>> finalMinimizers;
    //backtrack parent minimizers of max lis minimizer
    while (maxIndex != -1)
    {
        finalMinimizers.push_back(arr[maxIndex]);
        maxIndex = parentsIndex[maxIndex];
    }
    reverse(finalMinimizers.begin(), finalMinimizers.end());
    return finalMinimizers;
}

//diffchecker
void getMutations(int firstMatchIndexReference, string reference, string mutated)
{

    for (int i = 0; i < reference.size(); i++)
    {
        if (reference[i] != mutated[i])
        {

            if (reference[i] == '-')
                outputFile << "I," << firstMatchIndexReference + i << "," << mutated[i] << endl;
            else if (mutated[i] == '-')
                outputFile << "D," << firstMatchIndexReference + i << ",-" << endl;
            else
                outputFile << "X," << firstMatchIndexReference + i << "," << mutated[i] << endl;
        }
    }
}

//align sequences based on  lis minimizers
void align(vector<tuple<string, int, int>> lisValue, string referenceGenome, string mutationSequence)
{
    //indexes only in their own strings, non aligned
    int firstMatchIndexReference = get<1>(lisValue[0]);
    int lastMatchIndexReference = get<1>(lisValue[lisValue.size() - 1]) + k - 1;

    int firstMatchIndexMutation = get<2>(lisValue[0]);
    int lastMatchIndexMutation = get<2>(lisValue[lisValue.size() - 1]) + k - 1;

    string newString1;
    string newString2;
    int currentIndex = 0;

    //allignment
    for (int i = firstMatchIndexReference, j = firstMatchIndexMutation; i <= lastMatchIndexReference;)

    {

        if (get<1>(lisValue[currentIndex]) == i && get<2>(lisValue[currentIndex]) == j)
        {
            newString1 += mutationSequence[j - 1];
            newString2 += mutationSequence[j - 1];
            i++;
            j++;
            currentIndex++;
        }
        else if (get<1>(lisValue[currentIndex]) == i)
        {
            newString1 += "-";
            newString2 += mutationSequence[j - 1];
            j++;
        }
        else if (get<2>(lisValue[currentIndex]) == j)
        {
            newString1 += referenceGenome[i - 1];
            newString2 += "-";
            i++;
        }
        else
        {
            //-1 because here the starting index is 0, not 1 (1.st position)
            newString1 += referenceGenome[i - 1];
            newString2 += mutationSequence[j - 1];
            i++;
            j++;
        }
    }
    getMutations(firstMatchIndexReference, newString1, newString2);
}

bool isNotAlnum(char c)
{
    return isalnum(c) == 0;
}

int main()
{

    //read reference genome lines
    ifstream infile("lambda.fasta");
    string line;
    getline(infile, line);
    while (getline(infile, line))
    {
        referenceGenome += line;
    }

    //read mutation sequence lines
    ifstream infile2("lambda_simulated_reads.fasta");
    while (getline(infile2, line))
    {
        getline(infile2, line);
        mutationSequences.push_back(line);
    }

    infile.close();
    infile2.close();

    //currently testing for first mutation sequence

    set<pair<string, int>> referenceMinimizers = calculateMinimizers(referenceGenome);

    int counter = 1;
    for (string sequence : mutationSequences)
    {

        cout << (counter++) << "/" << mutationSequences.size() << endl;
        set<pair<string, int>> mutationMinimizers = calculateMinimizers(sequence);
        vector<tuple<string, int, int>> matchesMap = sortedMatching(referenceMinimizers, mutationMinimizers);
        vector<tuple<string, int, int>> lisValue;

        if (matchesMap.size() != 0)
            lisValue = lis(matchesMap);

        if (lisValue.size() != 0)
            align(lisValue, referenceGenome, sequence);
    }

    outputFile.close();
    return 0;
}